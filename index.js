import got from "got";
import jsdom from "jsdom";
const {JSDOM} = jsdom;

const scrapperData = async () => {
    got("https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP")
        .then(
            async (response) => {
                const dom = new JSDOM(response.body);
                const codeHTTP = dom.window.document.querySelectorAll("th[scope='row']");
                const description = dom.window.document.querySelectorAll("td > span > i");
                
                const data = [];
                codeHTTP.forEach(
                    async (code) => {
                        data.push({
                            code: code.textContent,
                            message: ''
                        });
                    }
                );
                description.forEach(
                    async (desc, index) => {
                        data[index].message = desc.textContent;
                    }
                );
                console.log(data);
                return dom;
            }
        )
        .catch((err) => {
            console.log(err);
        });
}

scrapperData();